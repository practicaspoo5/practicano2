package ito.poo.app;

import java.util.Scanner;


public class Practica2 {

	static Scanner input= new Scanner(System.in);
	//=============================================
	// Este método debe leer desde el teclado
	// cada uno de los elementos de un arreglo
	// que es pasado como argumento.
	//=============================================
	static void leerArreglo (int [] a) {
		
		System.out.println("Inserte el dia de entrega de las tareas:");
		for(int i=0; i<a.length; i++) {
			System.out.print("Tarea "+(i+1)+": ");
			a[i]=input.nextInt();
		}
		
	}
	
	//=============================================
	// Este método debe retornar el número de tareas
	// que han vencido en función del día (hoy) pasado
	// como argumento.
	// =============================================
	static int cantidaTareasVencidas(int [] a , int hoy) {
		int c=0;
		
		for(int i=0; i<a.length; i++) {
			if(a[i]<=hoy) {
				c++;
			}
		}
		
		return c;
	}
	// =============================================
	// Este método debe retornar la lista de tareas que 
	// han vencido al día (hoy).
	// =============================================
	static int [] tareasVencidas(int [] a, int hoy) {
		int r= cantidaTareasVencidas(a, hoy);
		int[] lv= new int[r];
		
		for(int i=0; i<a.length; i++) {
			if(a[i]<=hoy) {
				lv[i]=a[i];
			}
		}
		
		return lv;
	}
	
	static int cantidadTareasProximas(int [] a, int hoy) {
		int c=0, b=0;
		b=hoy+5;
		for(int i=0; i<a.length; i++) {
			if(a[i]>hoy && a[i]<b) {
				c++;
			}
		}
		
		return c;
	}
	
	static int [] tareasProximas(int [] a, int hoy) {
		int r=0, b=0, c=0;
		r=cantidadTareasProximas(a, hoy);
		int[] lv= new int[r];
		b=hoy+5;
		for(int i=0; i<a.length; i++) {
			if(a[i]>hoy && a[i]<b) {
				lv[c]=a[i];
				c++;
			}
		}
		
		return lv;
	}
	
	static int cantidadTareasAReprogramar(int [] a, int hoy) {
		int c=0;
		
		for(int i=0; i<a.length; i++) {
			if(a[i]>(hoy+5)) {
				c++;
			}
		}
		
		return c;
	}
	
	static int [] tareasAReprogramar(int [] a, int hoy) {
		int r=0, c=0;
		r=cantidadTareasAReprogramar(a, hoy);
		int [] lv= new int[r];
		
		for(int i=0; i<a.length; i++) {
			if(a[i]>(hoy+5)) {
				lv[c]=a[i];
				c++;
			}
		}
		
		return lv;
	}
	
	static void imprimeResultados(int [] a) {
		if(a.length!=0) {
			System.out.print("[");
			for(int i=0; i<a.length; i++) {
				
				System.out.print(a[i]+",");
				
			}
			System.out.println("]");
		}else {
			
			System.out.println("No exsiste ninguna tarea");
			
		}
	}
	
	static void run() {
		int a[] = new int[6];
		int aux[] = {};
		int Dia = 3;
		leerArreglo(a);
		
		aux=tareasVencidas(a, Dia);
		System.out.print("Las tareas vencidas son: ");
		imprimeResultados(aux);
		
		aux=tareasProximas(a, Dia);
		System.out.print("Las tareas proximas son: ");
		imprimeResultados(aux);
		
		aux=tareasAReprogramar(a, Dia);
		System.out.print("Las tareas a reprogramar son: ");
		imprimeResultados(aux);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
